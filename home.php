<HTML>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <title>Invernastsa LowCost Hydroponic System - Home</title>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link rel="shortcut icon" href="./website/images/tomato.ico" type="image/x-icon" />

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>



    <!-- ROOM TEMP GAUGE -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Temp C', <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }
                    $sql = "SELECT temperature FROM measurements ORDER BY ttime DESC LIMIT 1";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["temperature"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?>

                ],

            ]);

            var options = {
                width: 400, height: 200,
                redFrom: 27, redTo: 100,
                yellowFrom: 20, yellowTo: 27,
                greenFrom: 0, greenTo: 20,
                minorTicks: 5
            };

            var chart = new google.visualization.Gauge(document.getElementById('roomtemp_div'));

            chart.draw(data, options);


        }
    </script>
    <!-- VIV 1 HUM GAUGE -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['HUM %', <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }

                    $sql = "SELECT humidity FROM measurements ORDER BY ttime DESC LIMIT 1";
                    //$sql = "SELECT humidity FROM datalogger where sensor = 8 ORDER BY date_time DESC LIMIT 1";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["humidity"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?> ],

            ]);

            var options = {
                width: 400, height: 200,
                redFrom: 60, redTo: 100,
                yellowFrom: 10, yellowTo: 60,
                minorTicks: 5
            };

            var chart = new google.visualization.Gauge(document.getElementById('roomhum_div'));

            chart.draw(data, options);


        }
    </script>

    <!-- ROOM TEMP GAUGE -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Temp C', <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }
                    $sql = "SELECT temperature FROM measurements_2 ORDER BY ttime DESC LIMIT 1";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["temperature"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?>

                ],

            ]);

            var options = {
                width: 400, height: 200,
                redFrom: 27, redTo: 100,
                yellowFrom: 20, yellowTo: 27,
                greenFrom: 0, greenTo: 20,
                minorTicks: 5
            };

            var chart = new google.visualization.Gauge(document.getElementById('roomtemp_div'));

            chart.draw(data, options);


        }
    </script>
    <!-- ROOM 2 HUM GAUGE -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['TEMP C', <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }

                    $sql = "SELECT temperature FROM measurements ORDER BY ttime DESC LIMIT 1";
                    //$sql = "SELECT humidity FROM datalogger where sensor = 8 ORDER BY date_time DESC LIMIT 1";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["temperature"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?> ],

            ]);

            var options = {
                width: 400,
                height: 200,
                redFrom: 30,
                redTo: 50,
                yellowFrom: 20,
                yellowTo: 30,
                greenFrom: 0,
                greenTo: 20,
                minorTicks: 5,
                greenColor: '#E0E0E0',
                yellowColor: '#66CC00',
                redColor: '#FF56666'
            };

            var chart = new google.visualization.Gauge(document.getElementById('viv1temp_div'));

            chart.draw(data, options);


        }
    </script>

    <!-- ROOM 2 HUM GAUGE -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['HUM %', <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }

                    $sql = "SELECT humidity FROM measurements ORDER BY ttime DESC LIMIT 1";
                    //$sql = "SELECT humidity FROM datalogger where sensor = 8 ORDER BY date_time DESC LIMIT 1";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["humidity"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?> ],

            ]);

            var options = {
                width: 400,
                height: 200,
                redFrom: 90,
                redTo: 100,
                yellowFrom: 50,
                yellowTo: 90,
                greenFrom: 0,
                greenTo: 50,
                minorTicks: 5,
                greenColor: '#66B2FF',
                yellowColor: '#3399FF',
                redColor: '#B266FF'
            };


            var chart = new google.visualization.Gauge(document.getElementById('viv1hum_div'));

            chart.draw(data, options);


        }
    </script>


</head>
<?php
//Create the top menu
include "./website/php/top_menu.php"
?>
<body>
<div class="jumbotron">
    <div class="container">
        <?php include 'menu.php';?>
    </div>
</div>
<div class="container">
    <h3>CURRENT CONDITIONS</h3>
    <?php include 'time.php';?><hr>
    <div class="row">
        <div class="col-sm-3">
            <a href="/" title="BASE" alt="BASE">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">1</strong>
</span>
            </a>
            <div id="roomtemp_div""></div>
        <div id="roomhum_div""></div>
</div>
<div class="col-sm-3">
    <a href="1.php" title="VIV 1" alt="VIV 1">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">3</strong>
</span>
    </a>
    <div id="viv1temp_div"></div>
    <div id="viv1hum_div"></div>
</div>

</span>
    </a>

</div>
</div>

<hr>

<div class="container">
    <a href="1.php" title="VIV 1" alt="VIV 1">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">1</strong>
</span>
    </a>
    <div id="tank1tempgraph_div" style="width: auto; height: 500px;"></div>
    <div id="tank1humgraph_div" style="width: auto; height: 500px;"></div>
</div>
<div class="container">
    <a href="2.php" title="VIV 2" alt="VIV 2">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">2</strong>
</span>
    </a>
    <div id="tank2tempgraph_div" style="width: auto; height: 500px;"></div>
    <div id="tank2humgraph_div" style="width: auto; height: 500px;"></div>
</div>
<div class="container"><hr>
    <?php include 'footer.php';?></div>
</BODY>
</HTML>

