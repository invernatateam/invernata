<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
            padding: 5px;
        }

        th {text-align: left;}
    </style>
</head>
<body>

<?php
$q = intval($_GET['q']);

$con = mysqli_connect('localhost','datalogger','datalogger','datalogger');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

mysqli_select_db($con,"ajax_demo");
$sql="SELECT * FROM irrigation WHERE ttime > NOW() - INTERVAL 24 HOUR";
$result = mysqli_query($con,$sql);

echo "<table>
<tr>
<th>Hora</th>
<th>Contador</th>
<th>Litros</th>
</tr>";
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['ttime'] . "</td>";
    echo "<td>" . $row['count'] . "</td>";
    echo "<td>" . $row['flow'] . "</td>";
    echo "</tr>";
}
echo "</table>";
mysqli_close($con);
?>
</body>
</html>