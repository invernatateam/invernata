<?php
header('Content-Type: application/json');
$pdo=new PDO("mysql:dbname=datalogger;host=localhost","datalogger","datalogger");
//require "datalogger_connect.php";

switch($_GET['q']){
    // Buscar Último Dato
    case 1:
        $statement=$pdo->prepare("SELECT count,flow FROM irrigation ORDER BY ttime DESC LIMIT 0,1");
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);
        echo $json;
        break;

    // Buscar Todos los datos
    case 2:

        $statement=$pdo->prepare("SELECT flow FROM counter ORDER BY ttime DESC LIMIT 1");
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);
        echo $json;
        break;
}
?>
