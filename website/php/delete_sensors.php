<?php
// INITIAL_TIMER_DATA.PHP
require "php/datalogger_connect.php";
$numofdays = $_POST['newtime'];
// Delete the sensor readings for the selected timeframe
if ($numofdays > 1) {
    //$sql = mysqli_prepare($conn, "DELETE FROM sensors WHERE timestamp < CURRENT_DATE - INTERVAL 0 DAY");
    $sql = mysqli_prepare($conn, "DELETE FROM measurements WHERE ttime < CURRENT_DATE - INTERVAL 0 DAY");
    mysqli_stmt_bind_param($sql, 's',$numofdays);
    mysqli_stmt_execute($sql);
    mysqli_close($conn);
}
elseif ($numofdays < 1) {
    // Delete all of the readings from the sensors table
    $sql = ("DELETE FROM datalogger");
    mysqli_query($conn, $sql);
    mysqli_close($conn);
}
?>
