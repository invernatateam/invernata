<!--?PHP
require_once("./include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
    exit;
}
?-->
<html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
<title>HomeGarden Sistema</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="jumbotron">
<div class="container">
<?php include 'menu.php';?>
</div>
</div>
<div class="container">
<h3>Invernata 1.0.2</h3>

<p>Esta aplicacion ha sido disenada por <a href=#>invernata</a> licencia de uso publico <strong>No puede ser vendida</strong>.</p>
<p>Si usted ha pagado por la aplicacion contacte con  <a href="#">nosotros</a> y haganoslo saber!</p>


<div class="container">
<a href="change-pwd.php" tooltip title="CHANGE PASSWORD" alt="CHANGE PASSWORD">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-key fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="logout.php" tooltip title="LOGOUT" alt="LOGOUT">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-sign-out fa-stack-1x fa-inverse"></i>
</span>
</a>
</div>
<div class="container"><hr>
<?php include 'footer.php';?>
</div>
</body>
</html>
