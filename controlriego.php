<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <title>Flujo de Riego</title>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>




    <!-- VIV 1 Real time asynchronus request AJAX-->
    <script type="text/javascript">
        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([

                ['Label', 'Value'],
                ['Litros Tot', 0],
                ['Flujo / min', 0],



            ]);
            var options = {
                width: 400,
                height: 200,
                redFrom: 90,
                redTo: 100,
                yellowFrom: 50,
                yellowTo: 90,
                greenFrom: 0,
                greenTo: 50,
                minorTicks: 5,
                greenColor: '#CCFFCC',
                yellowColor: '#FFFFCC',
                redColor: '#F78181'
            };
            var chart = new google.visualization.Gauge(document.getElementById('Medidores'));
            chart.draw(data, options);

            function getData () {
                $.ajax({
                    url: "http://zorreras.ddns.net:9000/homegarden/riego/datosensor.php?q=1",
                    success: function (response) {
                        data.setValue(0, 1, response[0].flow);
                        data.setValue(1, 1, response[0].count);
                        chart.draw(data, options);
                        setTimeout(getData, 5000);
                    }


                });

            }
            getData();
        }
        google.load('visualization', '1', {packages: ['gauge'], callback: drawChart});

    </script>

    <script>
        <!--tabla consulta-->

        function riegoTablas(str) {
            if (str=="") {
                document.getElementById("txtHint").innerHTML="";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function() {
                if (this.readyState==4 && this.status==200) {
                    document.getElementById("txtHint").innerHTML=this.responseText;
                }
            }
            xmlhttp.open("GET","riego/riegotablas.php?q="+str,true);
            xmlhttp.send();
        }
    </script>

</head>
<?php
//Create the top menu
include "./website/php/top_menu.php"
?>
<body>

<div class="jumbotron">
    <div class="container">
        <?php include 'menu.php'; ?>
        <h2>Flujo de riego Invernadero N. 8</h2>
        <?php include 'time.php'; ?>
    </div>
</div>
<div class="container">
    <h3>Riego en tiempo real</h3>
    <div class="row">
        <div class="col-sm-3">
            <div id="Medidores" style="width: 200px; height: 200px;"></div>
        </div>
    </div>
</div>
<hr>
</div>
<div class="container">
    <br>
    <br>
    <form>
        <select name="riegos" onchange="riegoTablas(this.value)">
            <option value="">Seleciona una opcion:</option>
            <option value="1">Riego ultimas 24H</option>
        </select>
    </form>
    <br>
    <div id="txtHint"><b>Informacion de los riegos 24h</b></div>
    <div id="chart_div" style="width: auto; height: 500px;"></div>
    <div id="chart2_div" style="width: auto; height: 500px;"></div>
    <div id="chart3_div" style="width: auto; height: 500px;"></div>
    <hr>
    <?php include 'footer.php'; ?>
</div>
</body>
</html>
