<?php

/* Include the `../src/fusioncharts.php` file that contains functions to embed the charts.*/


/* The following 4 code lines contains the database connection information. Alternatively, you can move these code lines to a separate file and include the file here. You can also modify this code based on your database connection.   */

$hostdb = "localhost";  // MySQl host
$userdb = "datalogger";  // MySQL username
$passdb = "datalogger";  // MySQL password
$namedb = "datalogger";  // MySQL database name

// Establish a connection to the database
$dbhandle = new mysqli($hostdb, $userdb, $passdb, $namedb);

/*Render an error message, to avoid abrupt failure, if the database connection parameters are incorrect */
if ($dbhandle->connect_error) {
    exit("There was an error with your connection: ".$dbhandle->connect_error);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <title>Medias EC y Ph</title>
    <link rel="shortcut icon" href="./website/images/tomato.ico" type="image/x-icon" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">


    <script src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
    <script src="http://static.fusioncharts.com/code/latest/fusioncharts.charts.js"></script>
    <script src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.zune.js"></script>
   


    <?php
    include("src/fusioncharts.php");
    ?>
    <?php

    // SELECT * FROM table
    //WHERE YEAR(date_created) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
    //AND MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)

    $strQuery = "SELECT * FROM ph_ec WHERE ttime >NOW() - INTERVAL 10 DAY";
    //$strQuery = "SELECT DISTINCT id, ph, ec FROM ph_ec; ";
    $result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
    if ($result) {

        $arrData = array(
            "chart" => array(
                "caption"=> "Ph y Conductividad por Meses",
                "subCaption"=> "(Ph y Conductividad analisis anual)",
                "xAxisname"=> "Mes",
                "yAxisName"=> "Relacion entre EC y TDS ",
                "numberPrefix"=> "%",
                "legendItemFontColor"=> "#666666",
                "theme"=> "zune"
            )
        );
        // creating array for categories object

        $categoryArray=array();
        $dataseries1=array();
        $dataseries2=array();
        $dataseries3=array();

        // pushing category array values
        while($row = mysqli_fetch_array($result)) {
            array_push($categoryArray, array(
                    "label" => $row["ttime"]
                )
            );
            array_push($dataseries1, array(
                    "value" => $row["ph"]
                )
            );

            array_push($dataseries2, array(
                    "value" => $row["ec"]
                )
            );




        }

        $arrData["categories"]=array(array("category"=>$categoryArray));
        // creating dataset object
        $arrData["dataset"] = array(array("seriesName"=> "EC en Plantas", "data"=>$dataseries1), array("seriesName"=> "EC en Maquina",  "renderAs"=>"line", "data"=>$dataseries2));


        /*JSON Encode the data to retrieve the string containing the JSON representation of the data in the array. */
        $jsonEncodedData = json_encode($arrData);

        // chart object
        $msChart = new FusionCharts("mscombi2d", "chart1" , "600", "350", "chart-container", "json", $jsonEncodedData);

        // Render the chart
        $msChart->render();

        // closing db connection
        $dbhandle->close();

    }

    ?>



</head>
<?php
//Create the top menu
include "./website/php/top_menu.php"
?>
<body>
<div class="jumbotron">
    <div class="container">
        <?php include 'menu.php'; ?>
        <h2>Ph y Ec de la Maquina Prisma e Invernata</h2>


        <?php include 'time.php'; ?>
    </div>
</div>

<div class="container"></div>
<!-- Create custom menu for selecting the timeframe for plotting the graphs -->
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar2" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">View Chart for:</a>
            </div>
            <div id="navbar2" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(1);">1 Day</a></li>
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(7);">1 Week</a></li>
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(30);">1 Month</a></li>
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(90);">3 Months</a></li>
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(180);">6 Months</a></li>
                    <li><a type="button" href="#" onclick="insert_ph_ec_interval(365);">1 Year</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

</div>
</div>
<div class="container">
    <h3>Ph y Conductividad Mensual</h3>
    <div class="row">
        <div class="col-sm-3">

            <center>
                <div id="chart-container">Chart will render here!</div>
            </center>
        </div>
        <div class="col-sm-3">
            <div id="charthum_div" style="width: 200px; height: 200px;"></div>
        </div>
    </div>
    <hr>
</div>
<div class="container">
    <div id="chart2_div" style="width: auto; height: 500px;"></div>
    <div id="chart_div" style="width: auot; height: 500px;"></div>
    <hr>
    <?php include 'footer.php'; ?>
</div>


</body>
</html>
