<?php
echo '
<a href="home.php" tooltip title="HOME" alt="HOME">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-home fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="1.php" title=" 1" alt=" 1">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">1</strong>
</span>
</a>
<a href="2.php" title=" 2" alt=" 2">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">2</strong>
</span>
</a>
<a href="3.php" title="CONTROL METEO" alt="CONTROL METEO">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">3</strong>
</span>
</a>
<a href="controlriego.php" title="CONTROL RIEGO" alt="CONTROL RIEGO">
<span class="fa-stack fa-3x">
   <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x fa-stack-text fa-inverse">4</strong>
</span>
</a>
<a href="javascript:history.go(0);" title="RELOAD" alt="RELOAD">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="buttons.php" title="MANUAL CONTROLS" alt="MAN CTRLS">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-th fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="settings.php" title="SETTINGS/ABOUT" alt="SETTINGS">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-cog fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="website/settings.php" title="CONFIGURATION/SHUTDOWN" alt="SETTINGS">
<span class="fa-stack fa-3x">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-wheelchair fa-stack-1x fa-inverse"></i>
</span>
</a>
';
?>

