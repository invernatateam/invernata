CREATE DATABASE hydropi;

use hydropi;

CREATE TABLE timer_override (
pk INT UNSIGNED PRIMARY KEY,
relay_1 TEXT,
relay_2 TEXT,
relay_3 TEXT,
relay_4 TEXT
);

INSERT INTO timer_override (pk, relay_1, relay_2, relay_3, relay_4) VALUES (1,”on”,”off”,”auto”,”on”);

SELECT * FROM timeoverride;

CREATE TABLE relay_1_timer (
pk INT UNSIGNED PRIMARY KEY,
starttime DATETIME,
stoptime DATETIME
);

INSERT INTO relay_1_timer (pk, starttime, stoptime)
VALUES (1, ‘2016-01-01 08:00:00’, ‘2016-03-31 15:30:00’),
(2, ‘2016-04-01 10:00:00’, ‘2016-06-30 13:00:00’),
(3, ‘2016-07-01 13:00:00’, ‘2016-09-30 19:00:00’),
(4, ‘2016-10-01 02:30:00’, ‘2016-12-31 06:00:00’);



REPEAT THE ABOVE 11 LINES FOR EACH TIMER

# buckup database from remote server

mysqldump -uroot -ppass recipes > recipes.sql
