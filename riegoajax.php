<!--?PHP
require_once("./include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
    exit;
}
?-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <title>Garden at Home</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">


    <!-- VIV 1 Real time asynchronus request AJAX-->

        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([

                ['Label', 'Value'],
                ['Temperature', 0],
                ['Humidity', 0],
                ['Total', 0]


        ]);
            var options = {
                width: 400, height: 400,
                redFrom: 90, redTo: 100,
                yellowFrom:75, yellowTo: 90,
                minorTicks: 5
            };
            var chart = new google.visualization.Gauge(document.getElementById('Medidores'));
            chart.draw(data, options);
            setInterval(function() {
                var JSON=$.ajax({
                    url:"http://www.zorreras.club/html/homegarden/DatosSensores.php?q=1",
                    dataType: 'json',
                    async: false}).responseText;
                var Respuesta=jQuery.parseJSON(JSON);


                data.setValue(0, 1,Respuesta[0].temperature);
                data.setValue(1, 1,Respuesta[0].humidity);
                data.setValue(2, 1,Respuesta[0].total);
                chart.draw(data, options);
            }, 1300);

        }
    </script>

    <!-- VIV 1 COUNT GAUGE -->

    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["gauge"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Riegos',
                    <?php
                    $servername = "localhost";
                    $username = "datalogger";
                    $password = "datalogger";
                    $dbname = "datalogger";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $dbname);
                    // Check connection
                    if (!$conn) {
                        die("Connection failed: " . mysqli_connect_error());
                    }

                    $sql = "SELECT (`total`) FROM `total` LIMIT 0, 30 ";


                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        // output data of each row
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo $row["total"];
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn);
                    ?> ],

            ]);

            var options = {
                width: 400, height: 200,
                minorTicks: 5
            };

            var chart = new google.visualization.Gauge(document.getElementById('chartcount_div'));

            chart.draw(data, options);


        }
    </script>

    <!-- VIV 1 HUM GRAPH -->
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['TIME', 'HUMIDITY',],
                <?php
                $db = mysql_connect("localhost", "datalogger", "datalogger") or die("DB Connect error");
                mysql_select_db("datalogger");

                $q = "select * from datalogger ";
                $q = $q . "where sensor = 8 ";
                $q = $q . "order by date_time desc ";
                $q = $q . "limit 60";
                $ds = mysql_query($q);

                while ($r = mysql_fetch_object($ds)) {
                    echo "['" . $r->date_time . "', ";
                    echo " " . $r->humidity . " ],";

                }
                ?>
            ]);

            var options = {
                title: 'HUMIDITY LAST HOUR',
                curveType: 'function',
                legend: {position: 'none'},
                hAxis: {textPosition: 'none', direction: '-1'},
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>

    <!-- VIV 1 TEMP GRAPH -->

    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['TIME', 'TEMP',],
                <?php
                $db = mysql_connect("localhost", "datalogger", "datalogger") or die("DB Connect error");
                mysql_select_db("datalogger");

                $q = "select * from datalogger ";
                $q = $q . "where sensor = 8 ";
                $q = $q . "order by date_time desc ";
                $q = $q . "limit 60";
                $ds = mysql_query($q);

                while ($r = mysql_fetch_object($ds)) {
                    echo "['" . $r->date_time . "', ";
                    echo " " . $r->temperature . " ],";

                }
                ?>
            ]);

            var options = {
                title: 'TEMP LAST HOUR',
                curveType: 'function',
                legend: {position: 'none'},
                hAxis: {textPosition: 'none', direction: '-1'},
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart2_div'));

            chart.draw(data, options);
        }
    </script>

    <!-- VIV 1 COUNTER GRAPH -->

    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['TIME', 'TEMP',],
                <?php
                $db = mysql_connect("localhost", "datalogger", "datalogger") or die("DB Connect error");
                mysql_select_db("datalogger");

                $q = "select * from total ";
                //$q = $q . "where sensor = 8 ";
                //$q = $q . "order by date_time desc ";
                //$q = $q . "limit 60";
                $ds = mysql_query($q);

                while ($r = mysql_fetch_object($ds)) {
                    echo "['" . $r->date_time . "', ";
                    echo " " . $r->total . " ],";

                }
                ?>
            ]);

            var options = {
                title: 'COUNTER LAST HOUR',
                curveType: 'function',
                legend: {position: 'none'},
                hAxis: {textPosition: 'none', direction: '-1'},
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart3_div'));

            chart.draw(data, options);
        }
    </script>

</head>
<body>
<div class="jumbotron">
    <div class="container">
        <?php include 'menu.php'; ?>
        <h2>Jardin y Plantas</h2>
        <?php include 'time.php'; ?>
    </div>
</div>
<div class="container">
    <h3>Condiciones actuales</h3>
    <div class="row">
        <div class="col-sm-3">
            <div id="Medidores" style="width: 200px; height: 200px;"></div>
        </div>
    <!--div class="row">
        <div class="col-sm-3">
            <div id="charttemp_div" style="width: 200px; height: 200px;"></div>
        </div>
        <!--div class="col-sm-3">
            <div id="charthum_div" style="width: 200px; height: 200px;"></div>
        </--div>
        <!--div class="col-sm-3">
            <div id="chartcount_div" style="width: 200px; height: 200px;"></div-->
        </div>
    </div>
    <hr>
</div>
<div class="container">
    <div id="chart1_div" style="width: auto; height: 500px;"></div>
    <!--div id="chart2_div" style="width: auto; height: 500px;"></div>
    <!--div id="chart3_div" style="width: auto; height: 500px;"></div-->
    <hr>
    <?php include 'footer.php'; ?>
</div>
</body>
</html>
