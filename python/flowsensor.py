#!/usr/bin/env python

##############################################################################
#
# Written by Alex Gomez for the Raspberry Pi - 2017
#
# Website:
# Contact: by email
#
# Please feel free to use and modify this code for you own use.
#
# This program is designed to send an email with a subject line,
# an attachment, a sender, multiple receivers, Cc receivers and Bcc receivers.
# In addition it will also read a pre prepared file and use it's contents to
# create the body of the email
# I hope you are enjoy with this example and is usefull for you
#
#
#Python 3 program for flow meter
#'''This is a Python3 Program gor flow meter
#input on pin 19, GND and 5vcc pn rpi zero
#input 4.7k ohm res -> RPI pin 19 + 10k ohm -> GND RPI
#
#WARNING: No stopped flow warning. Only approximate.'''
################################################################################

import RPi.GPIO as GPIO
import time, sys
import MySQLdb

FLOW_SENSOR = 19

GPIO.setmode(GPIO.BCM)
GPIO.setup(FLOW_SENSOR, GPIO.IN, pull_up_down = GPIO.PUD_UP)

global count
count = 0

def countPulse(channel):
   global count
   if start_counter == 1:
      count = count+1

      flow = count
      print(flow)

GPIO.add_event_detect(FLOW_SENSOR, GPIO.FALLING, callback=countPulse)

while True:
    try:
        start_counter = 1
        time.sleep(480)
        start_counter = 0
        flow = (count * 60 * 2.25 / 1000)
        print ("The flow is: %.3f Liter/min" % (flow))

        if flow > 1:

            #Open database connection
            db = MySQLdb.connect("localhost", "datalogger","datalogger","datalogger")
            curs = db.cursor()
            SQL = ('''INSERT INTO `irrigation` (`ttime`,`count`,`flow`) VALUES (NOW(),'%s','%i')'''%(count,flow))
            curs.execute(SQL)
            time.sleep(1)
            db.commit()
            print ("Data committed")

            count = 0

    except KeyboardInterrupt:
        print ('\ncaught keyboard interrupt!, bye')
        GPIO.cleanup()
        sys.exit()